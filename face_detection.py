import cv2

haarCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
image = cv2.imread("mateo.jpg")
grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detect faces in the image
faces = haarCascade.detectMultiScale(
    grayscale,
    scaleFactor=1.2,
    minNeighbors=5,
    minSize=(20, 20),
    flags=cv2.CASCADE_SCALE_IMAGE
)

# Draw a rectangle around the faces
for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imshow("Faces detection", image)
cv2.waitKey(0)

